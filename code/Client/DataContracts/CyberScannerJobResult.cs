﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace JobObjects
{
    public class SearchEngineTypeHelper
    {
        static public String GetDomain(SearchEngineType type)
        {
            switch (type)
            {
                case SearchEngineType.Bing:
                    return "bing.com";
                case SearchEngineType.Google:
                    return "google.com";
                case SearchEngineType.Yahoo:
                    return "yahoo.com";
				case SearchEngineType.Ask:
					return "ask.com";
				case SearchEngineType.Blekko:
					return "blekko.com";
				case SearchEngineType.Filestube:
					return "yahoo.com";
				case SearchEngineType.Icerocket:
					return "icerocket.com";
				case SearchEngineType.Baidu:
					return "baidu.com";
				case SearchEngineType.Soso:
					return "soso.com";
				case SearchEngineType.Sogou:
					return "sogou.com";
				case SearchEngineType.Gougou:
					return "gougou.com";
				case SearchEngineType.AOL:
					return "aol.com";
				case SearchEngineType.Lycos:
					return "lycos.com";
				case SearchEngineType.Gigablast:
					return "gigablast.com";
				case SearchEngineType.Yippy:
					return "yippy.com";
				case SearchEngineType.Teoma:
					return "teoma.com";
				case SearchEngineType.Yandex:
					return "yandex.com";
				case SearchEngineType.Forestle:
					return "forestle.com";
            }
            throw new Exception(String.Concat("Not supported SearchEngineType: ", type.ToString()));
        }
    }

    [DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    public enum SearchEngineType : byte
  {
		[EnumMember]
		Google = 0,
		[EnumMember]
		Bing = 1,
		[EnumMember]
		Yahoo = 2,
		[EnumMember]
		Ask = 3,
		[EnumMember]
		Blekko = 4,
		[EnumMember]
		Filestube = 5,
		[EnumMember]
		Icerocket = 6,
		[EnumMember]
		Baidu = 7,
		[EnumMember]
		Soso = 8,
		[EnumMember]
		Sogou = 9,
		[EnumMember]
		Gougou = 10,
		[EnumMember]
		AOL = 11,
		[EnumMember]
		Lycos = 12,
		[EnumMember]
		Gigablast = 13,
		[EnumMember]
		Yippy = 14,
		[EnumMember]
		Teoma = 15,
		[EnumMember]
		Yandex = 16,
		[EnumMember]
		Forestle = 17,
    }

    [DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    public enum SearchType : byte
    {
        [EnumMember]
        Web,
        [EnumMember]
        Image,
        [EnumMember]
        Audio,
        [EnumMember]
        Video,
    }

    [Serializable]
    [DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    public class CyberScannerJobResult
    {
        [DataMember]
        public int JobId { get; set; }

        [DataMember]
        public Guid JobInstanceId { get; set; }

        [DataMember]
        public SearchEngineType SearchEngine { get; set; }

        [DataMember]
        public SearchType SearchType { get; set; }

        [DataMember]
        public string SearchQuery { get; set; }

        [DataMember]
        public int ContentId { get; set; }

        [DataMember]
        public int DmcaCount { get; set; }

        [DataMember]
        public DateTime StartedAt { get; set; }

        [DataMember]
        public DateTime UpdatedAt { get; set; }

        [DataMember]
        public bool IsAvailabilityQuery { get; set; }

		[DataMember(IsRequired = false)]
		public string SearchEngineMirror { get; set; }
    }

    [DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    [Serializable]
    public class NonInfringingUrl
    {
        [DataMember]
		[StringLength(2048, ErrorMessage = "Non infringing link too long")]
		public string Url { get; set; }

        [DataMember]
        public Guid JobInstanceId { get; set; }

        [DataMember]
        public int UrlRank { get; set; }

		/// <summary>
		/// second-precision
		/// </summary>
		[DataMember(IsRequired = false)]
	    public DateTime DateFound { get; set; }
    }
}
