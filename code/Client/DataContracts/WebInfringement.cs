﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace JobObjects
{
    [Serializable]
    [DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    public class WebInfringement
    {
        /// <summary>
        /// ContentGroup's DateFound
        /// and 
        /// HostingDetail's DateFound (if first)
        /// </summary>
        [DataMember]
        public DateTime DateFound { get; set; }

        [DataMember]
        public DateTime? DateUserPosted { get; set; }

        [DataMember]
        public int ContentId { get; set; }

        [DataMember]
        [StringLength(500, ErrorMessage = "Host site too long")]
        public string MatchedTitle { get; set; }

        [DataMember]
        public float MatchedScore { get; set; }

        [DataMember]
        public string GroupId { get; set; }

        /// <summary>
        /// Job id which has found infringement.
        /// </summary>
        [DataMember]
        public int JobId { get; set; }

        /// <summary>
        /// Unique identifier of job's run which has found infringement.
        /// </summary>
        /// <remarks>
        /// Job id is not unique, because it might be run many times. this identifier lets precisely know which run has found infringement.
        /// </remarks>
        [DataMember]
        public Guid? JobInstanceId { get; set; }

        [DataMember]
        public int LinkSiteUrlRank { get; set; }

        [DataMember]
        [StringLength(2048, ErrorMessage = "Link site too long")]
        public string LinkSiteUrl { get; set; }

        [DataMember]
        [StringLength(2048, ErrorMessage = "Host site too long")]
        public string HostSiteUrl { get; set; }

        /// <summary>
        /// Hosted title's language.
        /// </summary>
        [DataMember]
        public string ContentLanguage { get; set; }

        [DataMember]
        public string Platform { get; set; }

		[Obsolete("Not persisted anymore. File size now is being extracted by Status Checkers.")]
		[DataMember(IsRequired = false)]
        public long Filesize { get; set; }

        [Obsolete("Not persisted")]
        [DataMember]
        public long ViewsCount { get; set; }

        [DataMember]
        [StringLength(500, ErrorMessage = "Uploader name too long")]
        public string UploaderName { get; set; }

        [Obsolete("not persisted")]
		[DataMember(IsRequired = false)]
        public string TorrentHash { get; set; }

        [DataMember]
        public int VideoLengthInSeconds { get; set; }

        /// <summary>
        /// HostingDetail's DateStatusModified
        /// </summary>
        [DataMember(IsRequired = false)]
        public DateTime? DateStatusModified { get; set; }

        /// <summary>
        /// HostingDetail's LinkStatus
        /// </summary>
        [DataMember(IsRequired = false)]
        public LinkStatus? UrlStatus { get; set; }

        /// <summary>
        /// Gets or sets the scanning effort ID that was used to find this infringement.
        /// </summary>
        [DataMember(IsRequired = false)]
        public int EffortId { get; set; }

		[StringLength(1024, ErrorMessage = "Linking's Html Title too long")]
		[DataMember(IsRequired = false)]
		public string LinkingHtmlTitle { get; set; }
    }
}
