﻿//NOTE: change VerificationStatus column description in [HostedTitle] table in links database project when modifying following enum

namespace JobObjects
{
	public enum InfringementStatus : byte
	{
		NotVerified = 0,
		Real = 1,
		Fake = 3,
	}
}
