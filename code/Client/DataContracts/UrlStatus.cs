﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace JobObjects
{
	//NOTE: change LinkStatus column description in [Hosting] table in links database project when modifying following enum
	[Serializable]
	[DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
	public enum LinkStatus : byte
	{
		[EnumMember]
		NotVerified = 1,
		[EnumMember]
		AutoVerifiedOffline,
		[EnumMember]
		AutoVerifiedOnline,
		[EnumMember]
		AutoVerifiedUnableToVerify,
		[EnumMember]
		ManuallyVerifiedOnline,
		[EnumMember]
		ManuallyVerifiedOffline,
	}

	[Serializable]
	[DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
	public class UrlStatus
	{
		[DataMember(IsRequired = false)]
		[Obsolete("Pass back UrlId instead. Will be removed soon.")]
		public string Url { get; set; }

		[DataMember]
		public LinkStatus Status { get; set; }

		[DataMember]
		public string StatusCheckDetails { get; set; }

		[DataMember]
		public long UrlId { get; set; }

		[StringLength(260, ErrorMessage = "File's name too long")]
		[DataMember(IsRequired = false)]
		public string FileName { get; set; }

		[StringLength(1024, ErrorMessage = "Hosting's Html Title too long")]
		[DataMember(IsRequired = false)]
		public string HostingHtmlTitle { get; set; }

		/// <summary>
		/// In bytes.
		/// </summary>
		[DataMember(IsRequired = false)]
		public long? FileSize { get; set; }
	}

	[DataContract(Namespace = "http://dtecnet.com/LinkSystem")]
	public class UrlData
	{
		[DataMember]
		public long UrlId { get; set; }

		[DataMember]
		public string Url { get; set; }
	}
}