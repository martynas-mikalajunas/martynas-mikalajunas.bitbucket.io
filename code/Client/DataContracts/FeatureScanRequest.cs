﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LinkSubmitter.Common.DataContracts
{
    [DataContract]
    public class FeatureScanRequest
    {
        [DataMember]
        public int ContentId { get; set; }
        [DataMember]
        public long HostedTitleId { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
}
