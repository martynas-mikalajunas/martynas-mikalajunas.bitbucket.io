﻿using System.Runtime.Serialization;

namespace JobObjects
{
    [DataContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    public class ReverseLookupUrl
    {
        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int ContentId { get; set; }
    }
}
