﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace LinkSubmitter.Common.DataContracts
{
    [DataContract]
	[Serializable]
    public class HostedTitleFeatures
    {
        [DataMember]
        public int ContentId { get; set; }

        [DataMember]
        public long HostedTitleId { get; set; }

        [DataMember]
		[XmlArray]
		[XmlArrayItem(typeof(FeatureInfo))]
        public List<FeatureInfo> Features { get; set; }
    }
}
