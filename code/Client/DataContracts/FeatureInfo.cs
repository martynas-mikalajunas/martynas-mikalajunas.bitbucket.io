﻿using System;
using System.Runtime.Serialization;

namespace LinkSubmitter.Common.DataContracts
{
    [DataContract]
	[Serializable]
    public class FeatureInfo
    {
        [DataMember]
        public string FeatureName { get; set; }
        [DataMember]
        public int FeatureValue { get; set; }
    }
}
