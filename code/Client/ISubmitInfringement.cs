﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

using JobObjects;
using LinkSubmitter.Common.DataContracts;

namespace LinkSubmitter.Common
{
    [ServiceContract(Namespace = "http://dtecnet.com/LinkSystem/")]
    public interface ISubmitInfringement
    {
        [OperationContract]
        string Ping();

        [OperationContract]
        void SubmitWebInfringements(IEnumerable<WebInfringement> webInfringements, InfringementStatus infringementStatus);

        [OperationContract]
        void UpdateStatus(IEnumerable<UrlStatus> statuses);

        [OperationContract]
		[Obsolete("use GetLinksForStatusCheck instead")]
        IEnumerable<string> GetLinksToCheck(int linksCount);

    	[OperationContract]
    	IEnumerable<UrlData> GetLinksForStatusCheck(int linkCount);
		
		[OperationContract]
        void SubmitNonInfringingLinks(IEnumerable<NonInfringingUrl> nonInfringingUrls);

        [OperationContract]
        void SubmitCyberscannerJobResult(IEnumerable<CyberScannerJobResult> cyberScannerJobResults);

        [OperationContract]
        IEnumerable<FeatureScanRequest> GetLinksForAutoVerification(int linkCount);

        [OperationContract]
        void UpdateVerificationStatus(IEnumerable<HostedTitleFeatures> statuses);

        [OperationContract]
        IEnumerable<ReverseLookupUrl> GetLinksForReverseLookup(int linksCount);
    }

}
