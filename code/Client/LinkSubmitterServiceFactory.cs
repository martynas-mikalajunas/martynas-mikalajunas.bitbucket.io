﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

using LinkSubmitter.Common.Settings;

namespace LinkSubmitter.Common
{
    /// <summary>
    /// Provides Link Submitter service client factory.
    /// </summary>
	public interface ISubmitInfringementClientFactory
	{
		/// <summary>
		/// creates instance of client
		/// </summary>
		/// <returns></returns>
		ISubmitInfringementClient Create();
	}

	
	public class LinkSubmitterServiceFactory : ISubmitInfringementClientFactory
    {

        #region Methods

        /// <summary>
        /// Creates Link Submitter service client.
        /// </summary>
        /// <returns>Link Submitter service client object.</returns>
        public ISubmitInfringementClient Create()
        {
            //* create service binding based on solution configuration
            Binding binding;

            //* create NET.TCP binding for deployment to testing or production enviroment
            binding = new NetTcpBinding(SettingsManager.Instance.ServiceSettings.SecurityMode);
            ((NetTcpBinding)binding).MaxReceivedMessageSize = SettingsManager.Instance.ServiceSettings.MaxReceivedMessageSize;

            binding.SendTimeout = TimeSpan.FromSeconds(SettingsManager.Instance.ServiceSettings.SendTimeoutSeconds);
            var address = new EndpointAddress(SettingsManager.Instance.ServiceSettings.EndpointAddress);

            //* create client object
            var client = new SubmitInfringementClient(binding, address);
            client.Endpoint.Contract.ContractType = typeof(ISubmitInfringement);

            //* set MaxItemsInObjectGraph property for each operation
            foreach (OperationDescription operation in client.Endpoint.Contract.Operations)
            {
                var behavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();

                if (behavior != null)
                {
                    behavior.MaxItemsInObjectGraph = SettingsManager.Instance.ServiceSettings.MaxItemsInObjectGraph;
                }
            }

            return client;
        }

        #endregion

    }
}
