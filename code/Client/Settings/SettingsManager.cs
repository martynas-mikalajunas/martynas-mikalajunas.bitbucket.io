﻿using System;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using System.Xml.Serialization;

namespace LinkSubmitter.Common.Settings
{
    /// <summary>
    /// A singleton class for managing settings.
    /// </summary>
    public class SettingsManager
    {

        #region Fields

        private static object syncRoot = new object();
        private static SettingsManager instance;

        private XmlSerializer serializer = new XmlSerializer(typeof(ServiceSettings));
        private ServiceSettings serviceSettings;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsManager"/> class.
        /// </summary>
        private SettingsManager()
        {
            LoadSettings();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the instance for this class.
        /// </summary>
        public static SettingsManager Instance
        {
            get
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {
                        instance = new SettingsManager();
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets service settings.
        /// </summary>
        public ServiceSettings ServiceSettings
        {
            get
            {
                return serviceSettings;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the settings.
        /// </summary>
        private void LoadSettings()
        {       
            try
            {
                string assemblyLocation = Assembly.GetEntryAssembly().Location;
                string exeName = Path.GetFileNameWithoutExtension(assemblyLocation);
                string workingDirectory = Path.GetDirectoryName(assemblyLocation);
                string settingsPath = Path.Combine(workingDirectory, String.Format("{0}.LinkSubmitterService.config", exeName));

                using (Stream stream = File.Open(settingsPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    serviceSettings = (ServiceSettings)serializer.Deserialize(stream);
                    stream.Close();
                    stream.Dispose();
                }
            }
            catch { }

            //* get default settings, is nothing was loaded
            if (serviceSettings == null)
            {
                serviceSettings = GetDefaultServiceSettings();
            }
        }

        /// <summary>
        /// Gets default service settings.
        /// </summary>
        /// <returns>Default service settings.</returns>
        private ServiceSettings GetDefaultServiceSettings()
        {
            ServiceSettings settings = new ServiceSettings();

            //TODO: playing with timeouts
            settings.SendTimeoutSeconds = (int)TimeSpan.FromMinutes(5).TotalSeconds;
            settings.MaxReceivedMessageSize = 20971520;
            settings.SecurityMode = SecurityMode.None;
            settings.MaxItemsInObjectGraph = 800000;
            settings.TitleChunkSize = 100;

#if DEBUG
			settings.EndpointAddress = "net.tcp://us-debug.dtecnet.com/LinkSystem.Submitter/Submit.svc";
#elif RELEASE_TESTING
			// from internal
			//settings.EndpointAddress = "net.tcp://us-debug/LinkSystem.Submitter/Submit.svc";
			settings.EndpointAddress = "net.tcp://us-debug.dtecnet.com/LinkSystem.Submitter/Submit.svc";
#elif RELEASE_PRODUCTION
			// for access from internal server, configure to access service by public IP (e.g. edit hosts to point to us-iis4)
            settings.EndpointAddress = "net.tcp://iis.ws.us.dtecnet.com:8002/LinkSystem/Submit.svc";
#endif

			return settings;
        }

        #endregion

    }
}
