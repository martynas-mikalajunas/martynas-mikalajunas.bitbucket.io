﻿using System.ServiceModel;

namespace LinkSubmitter.Common.Settings
{
    /// <summary>
    /// Provides Link Submitter service settings.
    /// </summary>
    public class ServiceSettings
    {

        #region Fields

        private int sendTimeoutSeconds;
        private int maxReceivedMessageSize;
        private SecurityMode securityMode;
        private int maxItemsInObjectGraph;
        private string endpointAddress;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceSettings"/> class.
        /// </summary>
        public ServiceSettings()
        {
            //* intentionally blank
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets send timeout in seconds.
        /// </summary>
        /// <value>
        ///     Send timeout in seconds.
        /// </value>
        public int SendTimeoutSeconds
        {
            get
            {
                return sendTimeoutSeconds;
            }
            set
            {
                sendTimeoutSeconds = value;
            }
        }

        /// <summary>
        /// Gets or sets maximal size of the received message.
        /// </summary>
        /// <value>
        ///     Maximal size of the received message.
        /// </value>
        public int MaxReceivedMessageSize
        {
            get
            {
                return maxReceivedMessageSize;
            }
            set
            {
                maxReceivedMessageSize = value;
            }
        }

        /// <summary>
        /// Gets or sets security mode.
        /// </summary>
        /// <value>
        ///     Security mode.
        /// </value>
        public SecurityMode SecurityMode
        {
            get
            {
                return securityMode;
            }
            set
            {
                securityMode = value;
            }
        }

        /// <summary>
        /// Gets or sets maximal number of items in object graph.
        /// </summary>
        /// <value>
        ///     Maximal number of items in object graph.
        /// </value>
        public int MaxItemsInObjectGraph
        {
            get
            {
                return maxItemsInObjectGraph;
            }
            set
            {
                maxItemsInObjectGraph = value;
            }
        }

        /// <summary>
        /// Gets or sets endpoint address.
        /// </summary>
        /// <value>
        ///     Endpoint address.
        /// </value>
        public string EndpointAddress
        {
            get
            {
                return endpointAddress;
            }
            set
            {
                endpointAddress = value;
            }
        }

        public int TitleChunkSize { get; set; }

        #endregion

    }
}
