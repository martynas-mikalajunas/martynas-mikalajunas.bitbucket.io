﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;

using JobObjects;
using LinkSubmitter.Common.DataContracts;

namespace LinkSubmitter.Common
{
	public interface ISubmitInfringementClient : IDisposable, ISubmitInfringement
	{
	    void Abort();
	}

	public class SubmitInfringementClient : ClientBase<ISubmitInfringement>, ISubmitInfringementClient
    {

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitInfringementClient"/> class.
        /// </summary>
        /// <param name="name">The name of the endpoint in the application configuration file.</param>
        public SubmitInfringementClient(string name)
            : base(name)
        {
            //* intentionally blank
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitInfringementClient"/> class.
        /// </summary>
        /// <param name="binding">The binding with which to make calls to the service.</param>
        /// <param name="address">The address of the service endpoint.</param>
        public SubmitInfringementClient(Binding binding, EndpointAddress address)
            : base(binding, address)
        {
            //* intentionally blank
        }

        #endregion

		#region ISubmitInfringement Members

		public string Ping()
		{
			return Channel.Ping();
		}

		public void SubmitWebInfringements(IEnumerable<WebInfringement> webInfringements, InfringementStatus infringementStatus)
		{
			Channel.SubmitWebInfringements(webInfringements, infringementStatus);
		}

		public void UpdateStatus(IEnumerable<UrlStatus> statuses)
		{
			Channel.UpdateStatus(statuses);
		}

		[Obsolete("use GetLinksForStatusCheck instead.")]
		public IEnumerable<string> GetLinksToCheck(int amount)
		{
			return Channel.GetLinksToCheck(amount);
		}

		public IEnumerable<UrlData> GetLinksForStatusCheck(int linkCount)
		{
			return Channel.GetLinksForStatusCheck(linkCount);
		}

		public void SubmitNonInfringingLinks(IEnumerable<NonInfringingUrl> nonInfringingUrls)
		{
			Channel.SubmitNonInfringingLinks(nonInfringingUrls);
		}

		public void SubmitCyberscannerJobResult(IEnumerable<CyberScannerJobResult> cyberScannerJobResults)
		{
			Channel.SubmitCyberscannerJobResult(cyberScannerJobResults);
		}

        public IEnumerable<FeatureScanRequest> GetLinksForAutoVerification(int linkCount)
        {
            return Channel.GetLinksForAutoVerification(linkCount);
        }

        public void UpdateVerificationStatus(IEnumerable<HostedTitleFeatures> statuses)
        {
            Channel.UpdateVerificationStatus(statuses);
        }

	    public IEnumerable<ReverseLookupUrl> GetLinksForReverseLookup(int linksCount)
	    {
	        return Channel.GetLinksForReverseLookup(linksCount);
	    }

	    void IDisposable.Dispose()
        {
            if (State == CommunicationState.Faulted)
            {
                Abort();
            }
            else
            {
                Close();
            }
        } 

	    #endregion
	}
}
