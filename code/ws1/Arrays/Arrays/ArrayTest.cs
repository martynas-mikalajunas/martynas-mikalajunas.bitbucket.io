﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace Arrays
{
	[TestFixture]
	public class ArrayTest
	{
		[Test]
		public void ShouldAddElementsInArray()
		{
			// arrange
			var a = (new[] {1, 2, 3, 4}).ToList();
			var b = (new[] {10, 20, 30}).ToList();

			var expected = (new[] {11, 22, 33, 4}).ToList();
			var adder = new Adder();

			// act 
			var result = adder.AddElements(a, b);

			// assert
			result.Should().BeEquivalentTo(expected);
		}
	}

	public class Adder
	{
		public IEnumerable<int> AddElements(List<int> lhs, List<int> rhs)
		{
			var result = new List<int>();

			int lesserSize = Math.Min(lhs.Count, rhs.Count);
			for (int i = 0; i < lesserSize; ++i)
				result.Add( lhs[i] + rhs[i]);

			var bigger = lhs.Count > rhs.Count ? lhs : rhs;

			result.AddRange(bigger.Skip(lesserSize));
			return result;
		}
	}
}